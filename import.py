#!/usr/bin/python
#
# Exports tickets from Zendesk and imports them into JIRA
# Author: Philip Colmer, philip.colmer@linaro.org
#
# Zendesk parameters
# Use multiple parameters if you share tickets between different ZD accounts. That allows the script
# to follow redirects that require re-authentication.
zd_auth = [
              ("<domain>.zendesk.com", "<username>", "<API token>")
          ]
#
# Specify the root URL for the Zendesk account you want to export from
zendesk_url = "https://<domain>.zendesk.com"
# Specify the name of the project in JIRA
jira_project_name = "My JIRA Project"
#
# JIRA parameters
#
# Specify the root URL for the JIRA instance
jira_url = "https://jira.example.com"
# The username to be used for the import. Recommended that a separate "import" account is used
jira_user = "import-tool"
# Password for the import tool
jira_password = "<password>"
# What issue type do you want used for the Zendesk issues?
jira_issue_type_name = "Ticket"
# What is the custom field ID for Service Desk Request Participants?
# The script can update/correct this if use_mysql is True.
jira_sd_request_participants = 10400
#
# Optionally improve import detail level by using MySQL to
# update certain fields. Functionality of the script is impared if
# this is disabled.
use_mysql = True
mysql_server = "localhost"
mysql_user = "jiradbuser"
mysql_pw = "<db password>"
mysql_db = "jiradb"
#
# Specify the maximum attachment size allowed in JIRA. This prevents the script from
# downloading attachments from Zendesk if they are too big to upload.
max_attachment_size = 10485760
#
# Do you only want to migrate closed tickets?
only_closed_tickets = False
#
# What are the email domains for internal users?
company_domains = ["example.com"]
#
# If you have some users (internal or external) that have raised tickets in Zendesk with
# multiple email addresses, you can map them here to a single email address.
email_map = {"fred@example.com":"fred.flintstone@example.com"
    }

# END OF CONFIGURATION

##############################################################

import sys
import requests
from requests.auth import HTTPBasicAuth
from requests.adapters import HTTPAdapter
import urllib
import urllib2
import urllib3
import ssl
import json
import MySQLdb as mdb
from datetime import datetime
import dateutil.parser
import getopt
from StringIO import StringIO
import time
import shutil
import os
from urlparse import urlparse
from decimal import *

# This function is used for debugging purposes when we want to make sure what
# the differences are between strings.
def assertEqualLongString(a, b):
   NOT, POINT = '-', '*'
   if a != b:
       print a
       o = ''
       for i, e in enumerate(a):
           try:
               if e != b[i]:
                   o += "%s %s vs %s\n" % (POINT, ord(e), ord(b[i]))
               else:
                   o += "%s %s vs %s\n" % (NOT, ord(e), ord(b[i]))
           except IndexError:
               o += 'X'

       o += NOT * (len(a)-len(o))
       if len(b) > len(a):
           o += POINT* (len(b)-len(a))

       print o
       print b

def ZendeskAuth(url):
    # Get the correct auth pair for the given URL. This allows us to cope with
    # redirects off to another ZD location
    parsed = urlparse(url)
    for i in zd_auth:
        # i is (url, username, token)
        if i[0] == parsed.netloc:
            return HTTPBasicAuth('%s/token' % i[1], i[2])
    print "FATAL ERROR: cannot match %s for Zendesk authentication" % url
    quit()

def ZendeskGet(partial_url):
    return ZendeskGetFull("%s/api/v2/%s" % (zendesk_url, partial_url))

def ZendeskGetFull(full_url):
    zd_auth = ZendeskAuth(full_url)
    while True:
        #print full_url

        r = requests.get(full_url, auth=zd_auth)
        #print r.status_code
        if r.status_code != 429:
            return r

        print "Zendesk rate limited - waiting to retry for %s" % r.headers['retry-after']
        print r.headers
        print r.text
        time.sleep(float(r.headers['retry-after']))
        print "... finished waiting"
        # and loop

def GetUserEmail(_id):
    global zd_name_dict

    # Check if we've cached the answer
    if _id in zd_name_dict:
        #print "User %s = %s" % (_id, zd_name_dict[_id])
        return zd_name_dict[_id]

    r = ZendeskGet("users/%s.json" % _id)
    if r.status_code == 200:
        user_json = json.loads(r.text)
        user = user_json['user']
        email = user['email']
        # Perform address remapping
        if email in email_map:
            #print "Remapping %s to %s" % (email, email_map[email])
            email = email_map[email]
        # Save away in the dictionary
        zd_name_dict[_id] = email

        return email
    else:
        print "Got error %s while getting retrieving users from Zendesk:" % r.status_code
        print r.text
    return ""

def GetJIRADate(zendesk_date):
    foo = datetime.strptime(zendesk_date, "%Y-%m-%dT%H:%M:%SZ")
    return foo.strftime("%Y-%m-%d %H:%M:%S")

def ConsistentString(foo):
    # Make sure that all \r are removed or replaced with \n
    # to keep things consistent.
    if not(foo is None):
        foo = foo.replace('\r\n', '\n')
        foo = foo.replace('\r', '')
    return foo

def CheckJIRAComments(jira_comments, zd_comment):
    # Returns JIRA comment ID if match on commenter and date
    # We know we always have at least ONE comment because we create
    # one ourselves to record details of the Zendesk ticket.
    commenter = GetUserEmail(zd_comment['author_id'])
    for jira_comment in jira_comments:
        if jira_comment['author']['name'] == commenter:
            # Although we save dates to JIRA using GetJIRADate,
            # JIRA gives US dates in what looks like ISO 8601 format.
            # So we need to get it back as a datetime and then compare
            # the strings.
            jira_create = dateutil.parser.parse(jira_comment['created']).strftime("%Y-%m-%d %H:%M:%S")
            if jira_create == GetJIRADate(zd_comment['created_at']):
                return jira_comment
    return None

def GetSDCommentPropertyID(comment_property_id):
    global last_known_property_bucket
    global current_sd_comment_bucket

    comment_property_id = int(comment_property_id)
    # JIRA gets IDs in blocks of 100 so we need to track the "bucket" that is currently being used
    # and use a corresponding ID from our own bucket of IDs.
    id_div = (comment_property_id / 100) * 100
    id_mod = comment_property_id % 100
    #print "Last known bucket = %s, JIRA bucket = %s" % (last_known_property_bucket, id_div)
    if last_known_property_bucket != id_div:
        last_known_property_bucket = id_div
        # Need to get a new bucket of IDs from the database
        try:
            con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
            with con:
                cur = con.cursor()
                cur.execute("SELECT SEQ_ID FROM SEQUENCE_VALUE_ITEM WHERE SEQ_NAME='EntityProperty'")
                result = cur.fetchone()
                current_sd_comment_bucket = int(result[0])
                cur.execute("UPDATE SEQUENCE_VALUE_ITEM SET SEQ_ID=SEQ_ID+100 WHERE SEQ_NAME='EntityProperty'")
                #print "Setting SD comment property bucket to %s" % current_sd_comment_bucket
        finally:
            if con:
                con.close()
    #print "Comment property ID = %s, SD comment property ID = %s" % (comment_property_id, current_sd_comment_bucket + id_mod)
    return current_sd_comment_bucket + id_mod

def SetCommentPrivacy(zd_public_comment, zd_date, jira_comment_id):
    if not(use_mysql):
        return

    # Service Desk uses JSON properties to say whether or not the comment is internal.
    sd_public_comment = {"internal": not(zd_public_comment)}
    json_property = json.dumps(sd_public_comment)
    print "... setting privacy for comment ID %s to %s" % (jira_comment_id, sd_public_comment)
    r = session.put("%s/rest/api/2/comment/%s/properties/sd.public.comment" % (jira_url, jira_comment_id), headers=headers, data=json_property, auth=jira_auth)
    if r.status_code != 200 and r.status_code != 201:
        print "... error while setting comment privacy: %s" % r.status_code
        print r.text
    else:
        # Service Desk stores two entity properties, both with a property key of sd.public.comment,
        # but the REST API only accesses the one with an entity name of CommentProperty. The other
        # one has an entity name of sd.comment.property so we have to either create it or update it.
        try:
            con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
            with con:
                cur = con.cursor()
                # Are there any entity properties for this comment?
                cur.execute("SELECT * FROM entity_property WHERE ENTITY_ID = %s", (jira_comment_id,))
                results = cur.fetchall()
                got_comment_property = -1
                got_sd_comment_property = -1
                for result in results:
                    db_ID = result[0]
                    db_ENTITY_NAME = result[1]
                    db_JSON_VALUE = result[6]
                    if db_ENTITY_NAME == "CommentProperty":
                        got_comment_property = db_ID
                    elif db_ENTITY_NAME == "sd.comment.property":
                        got_sd_comment_property = db_ID
                    # Check the JSON value and make sure it matches
                    json_value = json.loads(db_JSON_VALUE)
                    # Since Zendesk indicates if a comment is PUBLIC, if the JSON value matches then it is the
                    # wrong value because it needs to be the inverse.
                    if json_value['internal'] == zd_public_comment:
                        cur.execute("UPDATE entity_property SET json_value='%s' WHERE ID=%s" % (json_property, db_ID))
                # If we haven't found sd.comment.property, we need to create it, using ID+1 from CommentProperty.
                if got_comment_property != -1 and got_sd_comment_property == -1:
                    #print "... CommentProperty for comment ID %s is %s" % (jira_comment_id, got_comment_property)
                    cur.execute("INSERT INTO entity_property (ID, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, CREATED, UPDATED, json_value) VALUES (%s, '%s', %s, '%s', '%s', '%s', '%s')" %
                                (GetSDCommentPropertyID(got_comment_property), "sd.comment.property", jira_comment_id, "sd.public.comment", GetJIRADate(zd_date), GetJIRADate(zd_date), json_property))
        finally:
            if con:
                con.close()

def UploadAttachmentIfMissing(issue_id, jira_attachments, zd_attachment, zd_date, zd_author):
    # See if we've already uploaded this attachment. We use the filename and the datestamp for comparison.
    if not(jira_attachments is None):
        for ja in jira_attachments:
            # We need to compare against the encoded filename because if we've disabled MySQL modifications then the filename
            # retains the encodings and doesn't get converted back into the original Unicode.
            if ja['filename'] == zd_attachment['file_name'] or ja['filename'] == urllib.quote_plus(zd_attachment['file_name'].encode('utf-8')):
                jira_create = dateutil.parser.parse(ja['created']).strftime("%Y-%m-%d %H:%M:%S")
                if jira_create == zd_date:
                    return
                    # otherwise keep looping through the attachments to see if we have a match

    # Download the attachment from Zendesk - but only if it is small enough to then upload
    # to JIRA
    if zd_attachment['size'] > max_attachment_size:
        print "... skipping attachment %s because it is too big (%s > %s)" % (zd_attachment['file_name'], zd_attachment['size'], max_attachment_size)
        return

    print "... downloading %s" % zd_attachment['file_name']
    print "... URL = %s" % zd_attachment['content_url']
    zd_url = zd_attachment['content_url']
    # Set up a loop to allow us to handle redirection from Zendesk and switching authentication
    while not(zd_url is None):
        r = requests.get(zd_url, stream=True, auth=ZendeskAuth(zd_url), allow_redirects=False)
        if r.status_code == 200:
            zd_url = None # to get us out of the while loop
            with open('/tmp/%s' % zd_attachment['file_name'], 'wb') as out_file:
                for chunk in r.iter_content():
                    out_file.write(chunk)
        elif r.status_code == 302:
            zd_url = r.headers['location']
            print "... redirecting to %s" % zd_url
            # and loop
        else:
            print "... got error %s while downloading attachment" % r.status_code
            print r.headers
            return
    del r
    # and upload it to JIRA
    print "... uploading to JIRA"
    files = {'file': (urllib.quote_plus(zd_attachment['file_name'].encode('utf-8')), open('/tmp/%s' % zd_attachment['file_name'], 'rb'), zd_attachment['content_type'])}
    mp_header = {'X-Atlassian-Token': 'nocheck'}
    r = requests.post("%s/rest/api/2/issue/%s/attachments" % (jira_url, issue_id), headers=mp_header, files=files, auth=jira_auth)
    os.remove('/tmp/%s' % zd_attachment['file_name'])
    if r.status_code == 200:
        response = json.loads(r.text)
        if use_mysql:
            try:
                con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db,use_unicode=True,charset="utf8")
                with con:
                    cur = con.cursor()
                    # Escape any single quotes in the filename ...
                    fname = zd_attachment['file_name'].replace("'", "\\'")
                    #print "UPDATE fileattachment SET FILENAME='%s' WHERE ID=%s" % (fname, response[0]['id'])
                    cur.execute("UPDATE fileattachment SET FILENAME='%s' WHERE ID=%s" % (fname, response[0]['id']))
                    cur.execute("UPDATE fileattachment SET AUTHOR='%s' WHERE ID=%s" % (zd_author, response[0]['id']))
                    cur.execute("UPDATE fileattachment SET CREATED='%s' WHERE ID=%s" % (zd_date, response[0]['id']))
            finally:
                if con:
                    con.close()
    else:
        print "... got error %s while uploading attachment" % r.status_code
        print r.text

def ProcessTicket(ticket):
    # Extract the fields from the ticket ready to create it in JIRA
    subject = ticket['subject']
    description = ticket['description']
    status = ticket['status']
    requester_id = ticket['requester_id']
    assignee_id = ticket['assignee_id']
    collaborator_ids = ticket['collaborator_ids']

    # Tickets have been seen where JIRA reports the summary contains newlines so strip those out
    subject = subject.replace("\n","").replace("\r","")

    print "Processing Zendesk ticket %s" % ticket['id']

    if status != "closed" and status != "solved" and only_closed_tickets:
        print "... skipping ticket %s because status is %s" % (ticket['id'], status)
        return

    # Initialise issue_id so that we know to create a new issue if we don't match on one.
    issue_id = None

    # See if we've already imported this ticket
    query = urllib.quote('project="%s" and comment ~ "Imported from Zendesk %s"' % (jira_project_name, ticket['id']), '')
    r = requests.get('%s/rest/api/2/search?jql=%s' % (jira_url, query), auth=jira_auth)
    if r.status_code == 200:
        response = json.loads(r.text)
        if response['total'] > 0:
            issues = response['issues']
            issue = issues[0]
            issue_id = issue['id']
            print "... already exists as JIRA ticket %s" % issue['key']
            # JQL Search only returns navigable fields and we need to include attachments, so
            # re-fetch the issue.
            r = requests.get('%s/rest/api/2/issue/%s' % (jira_url, issue_id), auth=jira_auth)
            if r.status_code == 200:
                response = json.loads(r.text)
                issue_fields = response['fields']
            else:
                print "... got error %s when retrieving issue after search results" % r.status_code
                print r.text
                return
    else:
        print "... got error %s when searching JIRA for Zendesk issue" % r.status_code
        print r.text
        return

    if issue_id is None:
        # Do the minimum at this stage to create an issue
        new_ticket = {"fields": {}}
        new_ticket['fields'].update({'issuetype': {"id": jira_issue_type}})
        new_ticket['fields'].update({'project': {"id": jira_project}})
        new_ticket['fields'].update({'summary': subject})

        # Create the ticket now then re-fetch it so that we can fall into general code
        # that allows us to update tickets we've already created.
        json_ticket = json.dumps(new_ticket)
        r = session.post("%s/rest/api/2/issue" % jira_url, headers=headers, data=json_ticket, auth=jira_auth)
        if r.status_code != 201:
            print "Got error %s while creating issue:" % r.status_code
            print r.text
            return

        # Issue created - get the issue ID so that we can do further things with it
        response = json.loads(r.text)
        issue_id = response['id']
        print "... created new JIRA ticket %s" % response['key']

        # Update the created datestamp
        if use_mysql:
            try:
                con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
                with con:
                    cur = con.cursor()
                    cur.execute("UPDATE jiraissue SET CREATED='%s' WHERE ID=%s" % (GetJIRADate(ticket['created_at']), issue_id))
            finally:
                if con:
                    con.close()

        # Add a comment to show which Zendesk ticket this came from. This is used to ensure
        # we don't import more than once.
        new_comment = {"body": "Imported from Zendesk %s" % ticket['id']}
        json_comment = json.dumps(new_comment)
        r = session.post("%s/rest/api/2/issue/%s/comment" % (jira_url, issue_id), headers=headers, data=json_comment, auth=jira_auth)

        # Get the fields back from JIRA for this issue
        r = requests.get('%s/rest/api/2/issue/%s' % (jira_url, issue_id), auth=jira_auth)
        response = json.loads(r.text)
        issue_fields = response['fields']

    updated_ticket = {"fields": {}}
    # From here, we either have an existing ticket that we're looking to update,
    # or a new ticket where we haven't defined many of the fields.
    if issue_fields['summary'] != subject:
        print "... updating summary"
        updated_ticket['fields'].update({'summary': subject})
    if ConsistentString(issue_fields['description']) != ConsistentString(description):
        print "... updating description"
        updated_ticket['fields'].update({'description': description})
    requester_email = GetUserEmail(requester_id)
    if issue_fields['reporter'] is None or issue_fields['reporter']['name'] != requester_email:
        if not(requester_email is None):
            print "... updating requester to %s" % requester_email
            updated_ticket['fields'].update({'reporter': {"name": requester_email}})
    if assignee_id is None:
        if not(issue_fields['assignee'] is None):
            print "... clearing assignee"
        updated_ticket['fields'].update({'assignee': None})
    else:
        assignee_email = GetUserEmail(assignee_id)
        if issue_fields['assignee'] is None:
            print "... setting assignee"
        elif issue_fields['assignee']['name'] != assignee_email:
            print "... updating assignee"
        updated_ticket['fields'].update({'assignee': {"name": assignee_email}})

    # Update/add the collaborators
    collabs = []
    for collaborator in collaborator_ids:
        foo = GetUserEmail(collaborator)
        # Don't add as a collaborator if the requester - JIRA doesn't allow it
        if foo != requester_email:
            collabs.append({"name": foo})
    cf_name = 'customfield_%s' % jira_sd_request_participants
    updated_ticket['fields'].update({cf_name: collabs})

    # Write back the fields
    json_ticket = json.dumps(updated_ticket)
    r = session.put("%s/rest/api/2/issue/%s" % (jira_url, issue_id), headers=headers, data=json_ticket, auth=jira_auth)
    if r.status_code != 204:
        print "Got error %s while setting intermediate issue fields:" % r.status_code
        print r.text
        return()

    # Get comments back from JIRA for this issue
    r = requests.get('%s/rest/api/2/issue/%s/comment' % (jira_url, issue_id), auth=jira_auth)
    response = json.loads(r.text)
    jira_comments = response['comments']

    # Get comments back from Zendesk
    r = ZendeskGet("tickets/%s/comments.json" % ticket['id'])
    if r.status_code != 200:
        print "Got error %s while getting comments for Zendesk issue %s" % (r.status_code, ticket['id'])
        print r.text
    else:
        comments_json = json.loads(r.text)
        comments = comments_json['comments']
        # Zendesk treats the ticket description as the first comment so we have to skip it
        skipped_first = False
        for comment in comments:
            # For all comments, check for attachments
            if comment['attachments'] != []:
                for attach in comment['attachments']:
                    if "attachment" in issue_fields:
                        jira_attachments = issue_fields['attachment']
                    else:
                        jira_attachments = None
                    UploadAttachmentIfMissing(issue_id, jira_attachments, attach, GetJIRADate(comment['created_at']), GetUserEmail(comment['author_id']))
            if skipped_first:
                # If we are NOT using MySQL direct interactions, we do NOT upload private
                # comments because the REST API doesn't give us enough access to set the
                # appropriate bits for Service Desk comment privacy. Note that SD does comment
                # privacy VERY differently from JIRA comment privacy :-(.
                if comment['public'] == False and use_mysql == False:
                    print "... skipping comment %s because it is private and database interactions are turned off" % comment['id']
                else:
                    # Need to see if we've already published this Zendesk comment to JIRA.
                    # Can't rely on comment text not being edited so let's go off combination of
                    # author and datestamp. Should not be possible for mistakes that way given that
                    # datestamp is down to 1-second resolution.
                    published_comment = CheckJIRAComments(jira_comments, comment)
                    if published_comment is None:
                        if use_mysql:
                            new_comment = {"body": comment['body']}
                        else:
                            new_comment = {"body": "Comment from %s:\n%s" % (GetUserEmail(comment['author_id']), comment['body'])}
                        json_comment = json.dumps(new_comment)
                        r = session.post("%s/rest/api/2/issue/%s/comment" % (jira_url, issue_id), headers=headers, data=json_comment, auth=jira_auth)
                        if r.status_code == 201:
                            response = json.loads(r.text)
                            comment_id = response['id']
                            if use_mysql:
                                # Setting comment privacy REQUIRES MySQL involvement, which is why we only copy private
                                # comments if we are allowed to do MySQL munging ...
                                SetCommentPrivacy(comment['public'], comment['created_at'], comment_id)
                                # Comment successfully created so now do some MySQL munging to
                                # alter the author.
                                try:
                                    con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
                                    with con:
                                        cur = con.cursor()
                                        cur.execute("UPDATE jiraaction SET AUTHOR='%s' WHERE ID=%s" % (GetUserEmail(comment['author_id']), comment_id))
                                        cur.execute("UPDATE jiraaction SET UPDATEAUTHOR='%s' WHERE ID=%s" % (GetUserEmail(comment['author_id']), comment_id))
                                        cur.execute("UPDATE jiraaction SET CREATED='%s' WHERE ID=%s" % (GetJIRADate(comment['created_at']), comment_id))
                                        cur.execute("UPDATE jiraaction SET UPDATED='%s' WHERE ID=%s" % (GetJIRADate(comment['created_at']), comment_id))
                                finally:
                                    if con:
                                        con.close()
                        else:
                            print "Got error %s while creating comment:" % r.status_code
                            print r.text
                    else:
                        # Did the comment get edited?
                        if use_mysql:
                            temp_comment = comment['body']
                        else:
                            temp_comment = "Comment from %s:\n%s" % (GetUserEmail(comment['author_id']), comment['body'])
                        if use_mysql:
                            SetCommentPrivacy(comment['public'], comment['created_at'], published_comment['id'])
                        if ConsistentString(published_comment['body']) != ConsistentString(temp_comment):
                            print "... updating comment %s" % published_comment['id']
                            #assertEqualLongString(jira_comment, temp_comment)
                            revised_comment = {"body": temp_comment}
                            json_comment = json.dumps(revised_comment)
                            r = session.post("%s/rest/api/2/issue/%s/comment/%s" % (jira_url, issue_id, published_comment['id']), headers=headers, data=json_comment, auth=jira_auth)

                            if use_mysql and "updated_at" in ticket:
                                try:
                                    con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
                                    with con:
                                        cur = con.cursor()
                                        cur.execute("UPDATE jiraaction SET UPDATED='%s' WHERE ID=%s" % (GetJIRADate(ticket['updated_at']), published_comment['id']))
                                finally:
                                    if con:
                                        con.close()
            else:
                # This is the first Zendesk comment so ignore it.
                skipped_first = True

    # If we need to transition the ticket to a different state ...
    # Work out what state we WANT to be in
    if status == "new" or status == "open":
        desired_status = "Waiting for Support"
    elif status == "pending" or status == "hold":
        desired_status = "Waiting for Customer"
    elif status == "solved" or status == "closed":
        desired_status = "Resolved"
    else:
        print "Unexpected Zendesk status of %s" % status
        return

    # Now compare that with the current status to see if we need
    # to transition.
    if desired_status != issue_fields['status']['name']:
        # Get the possible transitions for the new ticket
        r = requests.get("%s/rest/api/2/issue/%s/transitions" % (jira_url, issue_id), auth=jira_auth)
        if r.status_code == 200:
            response = json.loads(r.text)
            trans = response['transitions']

            # Find the transition we need to use for this ticket's status
            trans_id = None
            for tran in trans:
                if tran['name'] == "Resolve this issue" and desired_status == "Resolved":
                    trans_id = tran['id']
                elif tran['name'] == "Respond to customer" and desired_status == "Waiting for Customer":
                    trans_id = tran['id']
                elif tran['name'] == "Respond to support" and desired_status == "Waiting for Support":
                    trans_id = tran['id']
            if trans_id is None:
                print "Can't match required transition for status %s" % status
            else:
                transition = {"transition": {"id": trans_id}}
                if desired_status == "Resolved":
                    # Add a resolution of "Fixed"; Zendesk doesn't have a resolution field so we'll have to go with this one.
                    transition.update({'fields': {'resolution': {"name": "Fixed"}}})
                json_transition = json.dumps(transition)
                r = session.post("%s/rest/api/2/issue/%s/transitions" % (jira_url, issue_id), headers=headers, data=json_transition, auth=jira_auth)
                if r.status_code != 204:
                    print "... error %s while transitioning issue" % r.status_code
                    print r.text
                else:
                    #print desired_status
                    #print "updated_at" in ticket
                    if use_mysql and desired_status == "Resolved" and "updated_at" in ticket:
                        try:
                            con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
                            with con:
                                cur = con.cursor()
                                cur.execute("UPDATE jiraissue SET RESOLUTIONDATE='%s' WHERE ID=%s" % (GetJIRADate(ticket['updated_at']), issue_id))
                        finally:
                            if con:
                                con.close()

    # Update the created datestamp
    if use_mysql and "updated_at" in ticket:
        try:
            con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
            with con:
                cur = con.cursor()
                cur.execute("UPDATE jiraissue SET UPDATED='%s' WHERE ID=%s" % (GetJIRADate(ticket['updated_at']), issue_id))
        finally:
            if con:
                con.close()

def ProcessSingleTicket(issue_id):
    r = ZendeskGet("tickets/%s.json" % issue_id)
    if r.status_code == 200:
        tickets_json = json.loads(r.text)
        ticket = tickets_json['ticket']
        ProcessTicket(ticket)
    else:
        print "Got error %s while requesting ticket %s from Zendesk" % (r.status_code, issue_id)
        print r.text

def CheckZendeskName(user):
    global names
    global zd_names

    if not(user is None):
        foo = GetUserEmail(user)
        if not(foo in names) and not(foo is None):
            print "Adding %s" % foo
            names.append(foo)

def GetNamesFromTicket(ticket):
    CheckZendeskName(ticket['requester_id'])
    CheckZendeskName(ticket['assignee_id'])
    for collab in ticket['collaborator_ids']:
        CheckZendeskName(collab)

def GetNamesFromSingleTicket(issue_id):
    r = ZendeskGet("tickets/%s.json" % issue_id)
    if r.status_code == 200:
        tickets_json = json.loads(r.text)
        ticket = tickets_json['ticket']
        GetNamesFromTicket(ticket)
    else:
        print "Got error %s while requesting ticket %s from Zendesk" % (r.status_code, issue_id)
        print r.text

def CheckNamesInJIRA():
    global names

    # Create a copy of the names list so that we can iterate through one and
    # remove from the other
    names_copy = list(names)

    for name in names_copy:
        r = requests.get("%s/rest/api/2/user?username=%s" % (jira_url, name), auth=jira_auth)
        if r.status_code == 200:
            name_json = json.loads(r.text)
            if name_json['active'] == True:
                print "Removing %s from list of names" % name
                names.remove(name)
            else:
                print "%s is not active in JIRA" % name
        else:
            print "Not matching %s in JIRA" % name

def AutoCreateAccounts():
    global names
    names_copy = list(names)
    for name in names_copy:
        # Split the name at the @ sign so that we can check the domain
        foo = name.split("@")
        if len(foo) == 2 and not(foo[1] in company_domains):
            # Create an account
            acc = {"name": name, "emailAddress": name, "displayName": name}
            json_acc = json.dumps(acc)
            r = session.post("%s/rest/api/2/user" % jira_url, headers=headers, data=json_acc, auth=jira_auth)
            if r.status_code != 201:
                print "Got error %s while creating user %s" % (r.status_code, name)
                print r.text
            else:
                print "Created account for %s" % name
                names.remove(name)

##############################################################

def main(argv):
    global session
    global jira_auth
    global headers
    global error_report
    global names
    global zd_name_dict
    global jira_project
    global jira_issue_type
    global last_known_property_bucket
    global current_sd_comment_bucket

    last_known_property_bucket = -1
    current_sd_comment_bucket = -1

    session = requests.Session()
    jira_auth = HTTPBasicAuth(jira_user, jira_password)
    headers = {'content-type': 'application/json'}

    error_report = StringIO()

    zendesk_issue = -1
    skip_name_collection = False
    zd_name_dict = dict()

    try:
        opts, args = getopt.getopt(argv, "st:")
    except getopt.GetoptError:
        print "import.py [-s] [-t ID]"
        print "  -s skips name collection from Zendesk issues"
        print "  -t ID processes JUST Zendesk issue ID"
        sys.exit()

    for opt, arg in opts:
        if opt == "-s":
            skip_name_collection = True
        if opt == '-t':
            zendesk_issue = arg

    # Find the project number for the given JIRA project name
    jira_project = -1
    r = requests.get("%s/rest/api/2/project" % jira_url, auth=jira_auth)
    if r.status_code != 200:
        print "Got error while fetching projects from JIRA: %s" % r.status_code
        print r.text
        quit()

    response = json.loads(r.text)
    for p in response:
        if p['name'] == jira_project_name:
            jira_project = p['id']
            break

    if jira_project == -1:
        print "Unable to match project name '%s'" % jira_project_name
        quit()

    # Find the issue type we want to use when creating "tickets" in Service Desk
    jira_issue_type = -1
    r = requests.get("%s/rest/api/2/issuetype" % jira_url, auth=jira_auth)
    if r.status_code != 200:
        print "Got error while fetching issue types from JIRA: %s" % r.status_code
        print r.text
        quit()

    response = json.loads(r.text)
    for p in response:
        if p['name'] == jira_issue_type_name:
            jira_issue_type = p['id']
            break

    if jira_project == -1:
        print "Unable to match issue type '%s'" % jira_issue_type_name
        quit()

    # If we're allowed to do database checks, get the correct ID for
    # sd-request-participants
    if use_mysql:
        try:
            con = mdb.connect(mysql_server,mysql_user,mysql_pw,mysql_db)
            with con:
                cur = con.cursor()
                cur.execute("SELECT ID FROM customfield WHERE CUSTOMFIELDTYPEKEY='com.atlassian.servicedesk:sd-request-participants'")
                results = cur.fetchall()
                if len(results) == 1:
                    jira_sd_request_participants = results[0][0]
                else:
                    print "Cannot find SD Request Participants custom field"
        finally:
            if con:
                con.close()

    if not(skip_name_collection):
        # Iterate through all of the Zendesk issues, collecting names
        # Once we have the names, we then query JIRA to see which ones
        # don't exist.
        names = []
        if zendesk_issue == -1:
            zd_url = "%s/api/v2/incremental/tickets.json?start_time=0" % zendesk_url
            r = ZendeskGet("incremental/tickets.json?start_time=0")
            while True:
                if r.status_code == 200:
                    tickets_json = json.loads(r.text)
                    tickets = tickets_json['tickets']
                    for ticket in tickets:
                        # The incremental API can return deleted tickets (which we
                        # don't want) and doesn't include all of the fields that we do
                        # want, so we need to fetch the full ticket.
                        if ticket['status'] != "deleted":
                            GetNamesFromTicket(ticket)
                    if tickets_json['next_page'] == None or tickets_json['next_page'] == zd_url or tickets_json['count'] < 1000:
                        print "Finished getting tickets"
                        break

                    # Fetch the next page of results and loop. Zendesk documentation says to only
                    # do this if the count = 1000
                    print "Getting next page of tickets"
                    zd_url = tickets_json['next_page']
                    r = ZendeskGetFull(tickets_json['next_page'])
                elif r.status_code == 422:
                    # Too recent start time. End of tickets
                    break
                else:
                    print "Got error while requesting tickets from Zendesk"
                    print r.status_code
                    print r.text
                    quit()
        else:
            GetNamesFromSingleTicket(zendesk_issue)

        # If we've collected names from Zendesk, remove the ones that are in JIRA
        # already.
        if names != []:
            CheckNamesInJIRA()
        # If we've still got some names, create accounts for the ones that don't
        # match the organisation's email address(es).
        if names != []:
            AutoCreateAccounts()
        # and if we've STILL got some names, print them out for manual resolution.
        # This is likely to be company people who use multiple email addresses.
        if names != []:
            print "The following email addresses cannot be matched in JIRA:"
            for name in names:
               print name
            quit()
    else:
        print "Skipping name collection"

    # Get the list of tickets from Zendesk
    # Use the incremental API and loop until we run out of pages
    if zendesk_issue == -1:
        zd_url = "%s/api/v2/incremental/tickets.json?start_time=0" % zendesk_url
        r = ZendeskGet("incremental/tickets.json?start_time=0")
        while True:
            if r.status_code == 200:
                tickets_json = json.loads(r.text)
                tickets = tickets_json['tickets']
                for ticket in tickets:
                    # The incremental API can return deleted tickets (which we
                    # don't want) and doesn't include all of the fields that we do
                    # want, so we need to fetch the full ticket.
                    if ticket['status'] != "deleted":
                        ProcessSingleTicket(ticket['id'])
                    else:
                        print "Skipping issue %s because it is deleted" % ticket['id']
                if tickets_json['next_page'] == None or tickets_json['next_page'] == zd_url or tickets_json['count'] < 1000:
                    break

                # Fetch the next page of results and loop
                zd_url = tickets_json['next_page']
                r = ZendeskGetFull(tickets_json['next_page'])
            elif r.status_code == 422:
                # Too recent start time. End of tickets
                break
            else:
                print "Got error while requesting tickets from Zendesk"
                print r.status_code
                print r.text
                quit()
    else:
        ProcessSingleTicket(zendesk_issue)

    errors = error_report.getvalue()
    if errors != "":
        print "Recap of errors:"
        print errors

if __name__ == "__main__":
   main(sys.argv[1:])
